package com.example.demo;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.example.demo.dao.DataListDao;
import com.example.demo.domain.DataList;
import com.example.demo.utils.Util;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    private DataListDao dataListDao;

    @Test
    void find() {
        System.out.println(dataListDao.findAll().size());
    }

    @Test
    void delAll() {
        System.out.println(dataListDao.findAll().size());
        dataListDao.deleteAll();
        System.out.println(dataListDao.findAll().size());
    }

    @Test
    void ins() {
        DataList dataList = new DataList();
        dataList.setDanwei("ss");
        dataListDao.save(dataList);
    }

    @Test
    void readIns() {
        String url = "C:\\Users\\vlees\\Desktop\\java-excel\\双流区源丰建筑机具租赁结算书改.xls";
        ExcelReader reader;
        //通过sheet名获取
        reader = ExcelUtil.getReader(FileUtil.file(url), "改");
        List<List<Object>> readAll = reader.read(7, 584);
        // moth list
        List<String> monthList = new ArrayList<>();
        for (List<Object> objectList : readAll) {
            if (objectList.get(2).toString().length() < 6 && objectList.get(4).toString().length() < 6) {
                System.out.println("无日期，数据作废");
            } else {
                DataList dataList = new DataList();
                dataList.setName(objectList.get(0).toString());
                dataList.setDanwei(objectList.get(1).toString());
                if (ObjectUtil.isNotEmpty(objectList.get(2))) {
                    dataList.setOuttime(Util.pDay(objectList.get(2).toString()));
                }
                if (ObjectUtil.isNotEmpty(objectList.get(3))) {
                    dataList.setOutnum(Double.parseDouble(objectList.get(3).toString()));
                }
                if (ObjectUtil.isNotEmpty(objectList.get(4))) {
                    dataList.setIntime(Util.pDay(objectList.get(4).toString()));
                }
                if (ObjectUtil.isNotEmpty(objectList.get(5))) {
                    dataList.setInnum(Double.parseDouble(objectList.get(5).toString()));
                }
                if (ObjectUtil.isNotEmpty(objectList.get(6))) {
                    dataList.setHsprice(Double.parseDouble(objectList.get(6).toString()));
                }
                if (ObjectUtil.isNotEmpty(objectList.get(7))) {
                    dataList.setBhsprice(Double.parseDouble(objectList.get(7).toString()));
                }
                if (ObjectUtil.isNotEmpty(dataList.getIntime())) {
                    dataList.setDays((int) Util.jsDay(dataList.getIntime()));
                    dataList.setHssum(dataList.getInnum() * dataList.getHsprice() * dataList.getDays());
                    dataList.setBhssum(dataList.getInnum() * dataList.getBhsprice() * dataList.getDays());
                } else {
                    dataList.setDays((int) Util.jsDay(dataList.getOuttime()));
                    dataList.setHssum(dataList.getOutnum() * dataList.getHsprice() * dataList.getDays());
                    dataList.setBhssum(dataList.getOutnum() * dataList.getBhsprice() * dataList.getDays());
                }
                dataListDao.save(dataList);
            }
        }
    }

    public static void readExcel() {
        String url = "C:\\Users\\vlees\\Desktop\\java-excel\\双流区源丰建筑机具租赁结算书改.xls";
        ExcelReader reader;
        //通过sheet名获取
        reader = ExcelUtil.getReader(FileUtil.file(url), "改");
        List<List<Object>> readAll = reader.read(7, 584);
        /*
         * 物品名称	单位	   出库		入库		日租金不含税价（元）	日租金含税价（元）	天数	金额不含税价(元） 金额含税价(元) 备注
         *            日期	数量	    日期	数量
         */
        // 架管	元/米	2019/2/21	6342.70 			0.013 	0.01339		0.0000 	0.00
        // moth list
        List<String> monthList = new ArrayList<>();
        for (List<Object> objectList : readAll) {
            // System.out.println(objectList);
            if (objectList.get(2).toString().length() >= 6 || objectList.get(4).toString().length() >= 6) {
                if (ObjectUtil.isNotEmpty(objectList.get(2))) {
                    // 入库
//                    System.out.println(objectList.get(2));
                    monthList.add(objectList.get(2).toString().substring(0, 7));
                } else if (ObjectUtil.isNotEmpty(objectList.get(4))) {
                    // 出库
//                    System.out.println(objectList.get(4));
                    monthList.add(objectList.get(4).toString().substring(0, 7));
                }
            } else {
                // 没有日期数据
                System.out.println(objectList.toString());
            }
        }
//        System.out.println(monthList.toString());
        // 去重
        List<String> monthsList = monthList.stream().distinct().collect(Collectors.toList());
        System.out.println(monthsList.toString());
        // 按月分组数据
        Map<String, List> monthData = new HashMap<>();
        List<List<List<Object>>> monthListData = new ArrayList<>();
        for (String s : monthsList) {
            List<List<Object>> emonthList = new ArrayList<>();
            for (List<Object> objectList : readAll) {
                if (objectList.get(2).toString().contains(s) || objectList.get(4).toString().contains(s)) {
                    emonthList.add(objectList);
                }
            }
            monthData.put(s, emonthList);
            monthListData.add(emonthList);
        }
        // 便利每月数据
        for (List<List<Object>> monthListDatum : monthListData) {
            // 月分
            String month = Util.jsMonth(monthListDatum);
            // 含税
            Double monthSui = 0.0;
            // 不含税
            Double monthNoSui = 0.0;
            // 某一月数据
            for (List<Object> objectList : monthListDatum) {
                // 计算没条数据费用
                long dayNum = 0;
                Double num = 0.0;
                if (ObjectUtil.isNotEmpty(objectList.get(2))) {
                    // 入库
                    dayNum = Util.jsDay(objectList.get(2).toString());
                    num = Double.parseDouble(objectList.get(3).toString());
                } else if (ObjectUtil.isNotEmpty(objectList.get(4))) {
                    // 出库
                    dayNum = Util.jsDay(objectList.get(4).toString());
                    num = Double.parseDouble(objectList.get(5).toString());
                }

//                if (num <= 0) {
//                    System.out.println("------------num:" + num + "------------------------------");
//                }
                // 含税
                Double p = Double.parseDouble(objectList.get(6).toString());
                Double t = num * p * dayNum;
                // 不含税
                Double pp = Double.parseDouble(objectList.get(7).toString());
                Double tt = num * pp * dayNum;
                if (p < 0 || pp < 0) {
                    System.out.println(p + "/" + pp);
                }
                // 月 总
                monthSui = monthSui + t;
                monthNoSui = monthNoSui + tt;
            }
//            System.out.println("月:" + month);
            System.out.println("$$$$ 月:" + month);
            System.out.println("--包含税: " + monthSui);
//            System.out.println("--不含税: " + monthNoSui);
        }
    }

}
