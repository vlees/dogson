package com.example.demo.dao;

import com.example.demo.domain.DataList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DataListDao extends JpaRepository<DataList, Long> {

//    @Query("update DataList a set a.actorEmail = ?1 where a.id = ?2")
//    int updateActorEmailById(String email, Long id);
}