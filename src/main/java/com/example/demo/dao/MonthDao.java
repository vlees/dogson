package com.example.demo.dao;

import com.example.demo.domain.Month;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonthDao extends JpaRepository<Month, Long> {


}