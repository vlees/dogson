package com.example.demo.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "month")
public class Month {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String time;

    @Column
    private Double sum;

    @Column
    private String type;

}