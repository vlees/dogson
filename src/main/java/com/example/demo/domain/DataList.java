package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "list")
public class DataList {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private String danwei;

    @Column
    private Date outtime;

    @Column
    private Double outnum;

    @Column
    private Date intime;

    @Column
    private Double innum;

    @Column
    private Double hsprice;

    @Column
    private Double bhsprice;

    @Column
    private Integer days;

    @Column
    private Double hssum;

    @Column
    private Double bhssum;

}