package com.example.demo.utils;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;

import java.util.Date;
import java.util.List;

public class Util {

    public static long jsDay(String dateStr) {
        Date date1 = DateUtil.parse(dateStr);
        Date date2 = DateUtil.endOfMonth(date1);
        long x = DateUtil.between(date1, date2, DateUnit.DAY);//两个时间间隔几天
        x = x + 1;
        if (x < 1) {
            System.out.println("!!!!!!!!!!!time:" + dateStr + x);
        }
        return x;
    }

    public static long jsDay(Date date) {
        Date date1 = DateUtil.endOfMonth(date);
        long x = DateUtil.between(date, date1, DateUnit.DAY);//两个时间间隔几天
        x = x + 1;
        if (x < 1) {
            System.out.println("!!!!!!!!!!!time:" + date + x);
        }
        return x;
    }

    public static Date pDay(String dateStr) {
        return DateUtil.parse(dateStr, "yyyy-MM-dd");
    }

    public static String jsMonth(List<List<Object>> monthListDatum) {
        String timeStr;
        if (ObjectUtil.isNotEmpty(monthListDatum.get(0).get(2))) {
            timeStr = monthListDatum.get(0).get(2).toString();
        } else {
            timeStr = monthListDatum.get(0).get(4).toString();
        }
        Date date = DateUtil.parse(timeStr);
        String format = DateUtil.format(date, "yyyy/MM");
        return format;
    }
}
